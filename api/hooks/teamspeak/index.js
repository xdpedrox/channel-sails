// const TeamSpeak = require('../../../teamspeak/index')
// loginname= "serveradmin", password= "w2T2KcGK"

module.exports = function connectToTeamSpeak(sails) {

  return {
    connectToTeamSpeak: function (callback) {
      var hook = this;

      hook.initAdapters();

      // sails.adapters.teamspeak = TeamSpeak;
      //
      // TeamSpeak.ts3.on("ready", () => {
      //   sails.log.verbose('Connection to TeamSpeak was succesfull!');
      //   callback();
      //
      // });
      //
      // TeamSpeak.ts3.on("error", error => {
      //   sails.log.error(error);
      //   callback();
      // });

    },

    initAdapters: function () {
      if (sails.adapters === undefined) {
        sails.adapters = {};
      }
    },

    // Run automatically when the hook initializes
    initialize: function (cb) {
      // var hook = this;
      // hook.connectToTeamSpeak(function () {
      //   cb();
      // });
    },
  };
};
