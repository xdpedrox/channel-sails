module.exports = {


  friendlyName: 'Serverinfo',


  description: 'Serverinfo teamspeak.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {

    // console.log()
    await sails.adapters.teamspeak.ts3.execute("clientlist", ["-ip"])
    .then(server => {
      console.log(server)
    })
    // All done.
    return;

  }


};
