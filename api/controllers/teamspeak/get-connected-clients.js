module.exports = {


  friendlyName: 'Get connected clients',


  description: 'fetched the clients connected to the teamspeak server based on the IP',


  inputs: {

  },

  exits: {

  },



  fn: async function (inputs) {
    // console.log(this.req)
    console.log(this.req.ip)
    return sails.adapters.teamspeak.Users.getConnectedUsersByIP("90.255.27.20")
    .then(clids => {
      return this.res.apisuccess('Successfully fetched connected clients', {clids});
    })
    .catch(err =>{
      return this.res.apifailed('An error occurred while fetching the clients', );
    })
    // All done.
  }

};
