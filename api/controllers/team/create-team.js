module.exports = {


  friendlyName: 'Create Team',


  description: 'Create team.',


  inputs: {

    name: {
      required: true,
      type: 'string',
      description: 'The team name.',
      example: 'GamingXP3'
    },

    password: {
      required: true,
      type: 'string',
      description: 'The topic from the contact form.',
      example: 'I want to buy stuff.'
    },

    clid: {
      required: true,
      type: 'number',
      // description: 'The TeamArea if that the channel is going to be created',
      example: 2
    },

    uuid: {
      required: true,
      type: 'string',
      description: "does the user want to be moved to the channel",
      example: "smgDpLgFrZmV2FrK/PwWvFCA2Pw="
    },

    move: {
      required: true,
      type: 'boolean',
      description: "does the user want to be moved to the channel",
      example: true
    }
  },


  exits: {

  },


  fn: async function (inputs) {
    inputs.areaId = 1;
    let topic = 'topic'
    let description = 'description'

    return Team.findOne({
        teamName: inputs.name
      })
      .then(team => {
        // If the team exist
        if (team) {
          throw 'A team with that name already exists.'
        }
      })
      // Fetching TeamArea
      .then(() => {
        return TeamAreas.findOne({
          id: inputs.areaId
        })
      })
      // Create Team
      .then(teamArea => {
        // Create Channels
        return sails.adapters.teamspeak.Channel.createChannels(teamArea, inputs.name, inputs.password, topic, description, inputs.clid, inputs.move)
          .then(clids => {
            // If the channel Ids aren't returned
            if (!clids) {
              throw 'Error creating the channels on TS'
            }

            // Save team to DB  
            return Team.create({
              teamName: inputs.name,
              areaId: inputs.areaId,
              channelOrder: teamArea.nextChannelNumber,
              channelSpacerNumber: teamArea.nextSpacerNumber,
              channelRootId: clids.mainChannelId,
              channelEmptySpacerId: clids.spacerEmptyId,
              channelSpacerBarId: clids.spacerBarId,
            }).fetch()
          })
      })
      // Update Team Area
      .then(team => {
        // Check if team is present
        if (!team) {
          throw 'Error creating the team'
        }

         return TeamAreas.updateOne({
            id: 1
          })
          .set({
            nextChannelNumber: (team.channelOrder + 1),
            nextSpacerNumber: (team.channelSpacerNumber + 1),
            lastChannelId: (team.channelSpacerBarId)
          })
          .then(teamArea => {
            if (!_.isNumber(teamArea.id)) {
              throw "Error Updating teamArea"
            }
            return team;
          })
          .catch(err => {
            throw err;
          });
      })
      // Give user Channel Admin
      .then(team => {

        // sails.helpers.
        return team;
      })
      // Add user to the Team.
      .then(team => {
        TeamUsers.create({
          teamAdminLevel: 1,
          userId: 1,
          teamId: team.id
        })
        .catch(err => {
          throw "failed to add user to the team"
        })
      })
      // JSON Response Success
      .then(() => {
        return this.res.apisuccess('Successfully created the team', {});
      })
      // JSON Response Fail
      .catch(err => {
        sails.log.error(err);
        return this.res.apifailed('Failed to create the team');
      })

  }


};
