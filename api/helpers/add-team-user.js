const config = require('../../teamspeak/config/ts3config');

module.exports = {


  friendlyName: 'Add User to a Team',


  description: '',


  inputs: {

    team: {
      required: true,
      type: {},
      description: "Object of the Team",
      example: {
        teamName: 'lol'
      }
    },

    user: {
      required: true,
      type: {},
      description: "Object of the Team",
      example: {
        teamName: 'lol'
      }
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {


    if (permission == 1) {
      channelGroupId = config.groupSettings.channelAdmin;

    } else if (permission == 2) {
      channelGroupId = config.groupSettings.channelAdmin;

    } else if (permission == 3) {
      channelGroupId = config.groupSettings.channelMod;

    } else if (permission == 4) {
      channelGroupId = config.groupSettings.channelMember;

    } else {
      return 5;
      // Invalid Input
    }

    //Set Channel Groups
    return sails.adapters.teamspeak.Channel.setChannelGroupbyUid(channelGroupId, inputs.team.mainChannelId, inputs.user.uuid)
      .then(data => {
        console.log(data)

        //If team has a servergroup, then add the user to it
        if (inputs.team.serverGroupId != null) {
          sails.adapters.teamspeak.Channel.setClientServerGroupByUid(inputs.user.uuid, inputs.team.serverGroupId)
            .catch(err => {
              throw 'addUserToTeam: setClientServerGroupByUid failed';
              // throw failedApiReply(err, 'addUserToTeam: setClientServerGroupByUid failed');
            })
        }
        return data;
      })
      .catch(err => {
        throw 'addUserToTeam: Failed updating the database';
        // throw failedApiReply(err, 'addUserToTeam: Failed updating the database');
      })
  }
};
