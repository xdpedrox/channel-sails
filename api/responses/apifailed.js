module.exports = function (message, mergeObject) {

    let req = this.req;
    let res = this.res;
    let response = {};
  
    const statusCode = 500;
  
    let result = {
      status: statusCode
    };
  
    // Optional message
    if (message) {
      result.message = message;
    }
    // console.log(mergeObject);

  
    return res.status(statusCode).json(result);
  
  }
  