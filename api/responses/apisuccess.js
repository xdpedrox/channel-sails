module.exports = function (message, mergeObject) {

  let req = this.req;
  let res = this.res;
  let response = {};

  const statusCode = 200;

  let result = {
    status: statusCode
  };

  // Optional message
  if (message) {
    result.message = message;
  }

  if (_.isObject(mergeObject)) {
    response = Object.assign(result, mergeObject);
  } else {
    response = result
  }
  
  //Return Page
  return res.status(statusCode).json(response);

}
