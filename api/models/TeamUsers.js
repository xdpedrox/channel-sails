module.exports = {

  attributes: {

    //Area of the Game 
    userId: {
      model: 'user',
      required: true,
    },

    teamId: {
      model: 'Team',
      required: true,
    },

    teamAdminLevel: {
      type: "number",
      defaultsTo: 0,
      min: 0,
      max: 5
    },
  }
}
