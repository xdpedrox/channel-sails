module.exports = {

  attributes: {

    areaName: {
      type: "string",
      required: true,
      unique: true,
      isNotEmptyString: true,
      description: 'The name of the Area in the teamspeak',
      example: 'Public Area'
    },

    nextChannelNumber: {
      type: "number",
      required: true,
      min: 0,
      isInteger: true,
      description: 'The next channel name number (that is going to be used to create a new team). Channels are numbered incrementally',
      example: 1
    },

    nextSpacerNumber: {
      type: "number",
      required: true,
      isInteger: true,
      description: 'The number to be used in the spacer. To ensure the names are unique',
      min: 0,
      example: 10000
    },

    lastChannelId: {
      type: "number",
      required: true,
      isInteger: true,
      description: 'The last channelId of that area. Channels created will be created after this channelId',
      min: 0,
      example: 10000
    },
  }
}
