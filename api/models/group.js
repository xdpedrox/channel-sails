module.exports = {

  attributes: {

    serverGroupId: {
      type: "number",
      defaultsTo: 0,
      min: 2,
      description: 'TeamSpeak Server Group id of the group in question',
      example: 5
    },

    serverGroupName: {
      type: "string",
      isNotEmptyString: true,
      required: true,
      minLength: 1,
      unique: true,
    },

    // serverGroupIcon: {
    //   type: "string",
    //   // require: true,
    //   minLength: 1
    // },

    // //1 Template / other numbers could be user for other types
    // groupType: {
    //   type: "string",
    //   // require: true,
    //   minLength: 1
    // },
  }
}
