/**
 * User.js
 *
 * A user who can log in to this application.
 */

module.exports = {

  attributes: {

    //Team Name
    adminLevel: {
      type: 'number',
      defaultsTo: 0,
      min: 0,
      max: 5,
      description: 'The Admin level of the user 0 - Normal User 5 - MAX POWA',
      example: 0
    },

    //Team Name
    realName: {
      type: 'string',
      required: true,
      minLength: 6,
      isNotEmptyString: true,
      description: 'Users real name',
      example: 'Joao'
    },

    status: {
      type: "string",
      isIn: ['ok', 'banned'],
      defaultsTo: 'ok',
      description: 'The Status of the user account, ok - fine / banned - account is unable to loggin',
      example: 'ok'
    },

    nickname: {
      type: 'string',
      required: true,
      isNotEmptyString: true,
      description: 'users Nick Name on the website',
      example: 'LEET1337'
    },

    tsUuid: {
      type: 'string',
      required: true,
      isNotEmptyString: true,
      minLength: 6,
      minLength: 20,
      description: 'TeamSpeak client UUID.',
      example: "IXP2h9eBkJDY9x7zUelhFwZKzB0="
    },

    createChannelCooldown: {
      type: 'number',
      description: 'Last time the channel was used. Stores a Timestamp.',
      example: 1502844074211
    },

  },

};
