module.exports = {

  attributes: {

    //Team Name
    teamName: {
      type: "string",
      required: true,
      unique: true,
      minLength: 3,
    },

    //Area of the Game 
    areaId: {
      model: 'TeamAreas',
      required: true,
      min: 0,
      isInteger: true,
    },

    //Order of the Channel
    channelOrder: {
      type: "number",
      required: true,
      min: 0,
      isInteger: true,
    },

    //Number of the Spacer to make multiple spacer of the same name.
    channelSpacerNumber: {
      type: "number",
      required: true,
      min: 0,
      isInteger: true,
    },

    //Id of the Main Channel
    channelRootId: {
      type: "number",
      required: true,
      min: 0,
      isInteger: true,
    },

    //Id of the empty Spacer 
    channelEmptySpacerId: {
      type: "number",
      required: true,
      min: 0,
      isInteger: true,
    },

    //If othe Spacer Bar
    channelSpacerBarId: {
      type: "number",
      required: true,
      min: 0,
      isInteger: true,

    },

    //Id of the Server Group of the Team
    serverGroupId: {
      type: "number",
      allowNull: true,
      isInteger: true,
      min: 0
    },

    //Free or In use?
    status: {
      type: "string",
      isIn: ['ok', 'free'],
      defaultsTo: 'ok',
    },

    //When can the channel Move 
    nextMoveCooldown: {
      type: 'number',
      description: 'The next time the channel is allowed to move. Stores a Timestamp',
      example: 1502844074211
    },

    nameChangeCooldown: {
      type: 'number',
      description: 'Next time the Team Name can be changed. Stores a Timestamp.',
      example: 1502844074211
    },


    //When was the channel last used.
    channelLastUsed: {
      type: 'number',
      description: 'Last time the channel was used. Stores a Timestamp.',
      example: 1502844074211
    },
  }
}