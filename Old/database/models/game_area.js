const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash')

var gameAreaSchema = new mongoose.Schema({
    
    areaName: {
        type: String,
        // require: true,
        minLength: 1
    },

    nextChannelNumber: {
        type: Number,
        // require: true,
        minLength: 1
    },	

    nextSpacerNumber: {
        type: Number,
        // require: true,
        minLength: 1
    },

    lastChannelId: {
        type: Number,
        // require: true,
        minLength: 1
    }, 	
});


//Create Model
module.exports = mongoose.model('gameArea', gameAreaSchema);
