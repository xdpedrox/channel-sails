
const ts3 = require('../tsConnect');

/**
 * Create New Team by Creating Channels of Claming
 * @param {String} name
 * @param {String} password
 * @param {String} ownerUuid
 * @param {Number} areaId
 * @param {boolean} move
 */
const createTeam = (name, password, memberObjectID, areaId, move) => {

    //Get top free Channel
    return getTopFreeChannel(areaId)
        .then(team => {

            //Get Members Database
            return Members.findById(memberObjectID)
                .then(member => {
                    //If there is a Free Team then Claim it
                    if (team != null) {
                        //Claim Channel
                        return claimChannels(name, password, member.uuid, team._id, move)
                            .then(() => {
                                //Add user to the Channel and Give Permissions
                                return addUserToTeam(team, member, 1)
                                    .then(updatedTeam => {
                                        return ApiReply('createChannels: Channels Sucessfully Claimed', updatedTeam)

                                    })
                                    .catch(err => {
                                        throw failedApiReply(err, 'createTeam: Failed to add to Team');
                                    })
                            })
                            .catch(err => {
                                throw failedApiReply(err, 'createTeam: Failed to Claim Channels');
                            })

                        //If not Create a new one
                    } else {
                        //Create Channel
                        return createChannels(name, password, member.uuid, areaId, move)
                            .then((newteam) => {
                                //Add user to the Channel and Give Permissions
                                return addUserToTeam(newteam.data, member, 1)
                                    .then(updatedTeam => {
                                        return ApiReply('createChannels: Channels Sucessfully Created', updatedTeam)


                                    })
                                    .catch(err => {
                                        throw failedApiReply(err, 'createTeam: Failed to add to Team');
                                    })
                            })
                            .catch(err => {
                                throw failedApiReply(err, 'createTeam: Failed to create Channels');
                            })

                    }

                })
                .catch(err => {
                    throw failedApiReply(err, 'createTeam: Failed fetch Member DB');
                })

        })
        .catch(err => {
            throw failedApiReply(err, 'createTeam: Failed to fetch Team DB');
        })


}



/**
 * Adds User to the Team (inc: ServerGroup, ChannelAdmin, DB...)
 * @param {string} teamObjectID - Team objectID from the Database
 * @param {String} memberObjectID - Member objectID from the Database
 * @param {Number} permission - 1 to 4, 1 = Founder, 4 = Member
 */
const changeUserPermissions = (team, member, permission) => {


    if (permission == 1) {
        channelGroupId = config.groupSettings.channelAdmin;

    } else if (permission == 2) {
        channelGroupId = config.groupSettings.channelAdmin;

    } else if (permission == 3) {
        channelGroupId = config.groupSettings.channelMod;

    } else if (permission == 4) {
        channelGroupId = config.groupSettings.channelMember;

    } else {
        return removeUserFromTeam(team, member)
            .catch(err => {
                throw failedApiReply(err, 'addUserToTeam: removeUserFromTeam failed');
            })
    }

    let teamMemberArray = team.members.filter(teamMember => _.isEqual(teamMember.memberId, member._id));
    //TODO Remove Code Duplication
    //if the user is not a member of the team
    if (_.isEmpty(teamMemberArray)) {

        return Teams.findByIdAndUpdate(team._id,
            {
                $push: {
                    members: {
                        memberId: member._id,
                        memberUuid: member.uuid,
                        permissions: permission
                    }
                }
            }, {new: true}
        ).then(newTeam => {
            //If team has a servergroup, then add the user to it
            if (team.serverGroupId != null) {
                setClientServerGroupByUid(member.uuid, team.serverGroupId)
                    .catch(err => {
                        throw failedApiReply(err, 'addUserToTeam: setClientServerGroupByUid failed');
                    })
            }


            //reset channelGroup permission on all channels
            return setChannelGroupbyUid(channelGroupId, team.mainChannelId, member.uuid)
                .then(() => {
                    return newTeam;
                })
                .catch(err => {
                    throw failedApiReply(err, 'addUserToTeam: Failed updating the database');
                })

        })
            .catch(err => {
                throw failedApiReply(err, 'addUserToTeam: Failed updating the database');
            })

        //If User is already in the Team Remove and Add
    } else {

        newTeamMembers = team.members.filter(teamMember => !_.isEqual(teamMember.memberId, member._id));
        teamMemberArray[0].permissions = permission;
        newTeamMembers.push(teamMemberArray[0]);

        return Teams.findByIdAndUpdate(team._id,
            {
                $set: {
                    members: newTeamMembers
                }
            }, {new: true})
            .then(newTeam => {
                //If team has a servergroup, then add the user to it
                if (team.serverGroupId != null) {
                    setClientServerGroupByUid(member.uuid, team.serverGroupId)
                        .catch(err => {
                            throw failedApiReply(err, 'addUserToTeam: setClientServerGroupByUid failed');
                        })
                }


                //reset channelGroup permission on all channels
                return setChannelGroupbyUid(channelGroupId, team.mainChannelId, member.uuid)
                    .then(() => {
                        return newTeam;
                    })
                    .catch(err => {
                        throw failedApiReply(err, 'addUserToTeam: Failed updating the database');
                    })

            })
            .catch(err => {
                throw failedApiReply(err, 'addUserToTeam: Failed updating the database');
            })


    }

    // adding the user to the team members array

}


/**
 * Removes user from the team. (inc: ServerGroup, ChannelAdmin, DB...)
 * @param {String} teamObjectID - Team objectID of the Database
 * @param {String} memberObjectID - Member objectID of the Database
 */
const removeUserFromTeam = (team, member) => {


    //set guest channelGroup permission on all channels
    return setChannelGroupbyUid(config.groupSettings.guestChannelGroup, team.mainChannelId, member.uuid)
        .then(info => {

            //fetch teams database and remove user
            return Teams.findByIdAndUpdate(team._id,
                {
                    $pull: {
                        members: {
                            memberId: member._id,
                        }
                    }
                }, {new: true})
                .then(team => {

                    return ApiReply('removeUserFromTeam: User Removed', team)
                })
                .catch(err => {
                    throw failedApiReply(err, 'removeUserFromTeam: Failed updating the db records');
                })
        })
        .then(reply => {
            //Remove ServerGroup if exist
            if (!_.isNull(team.serverGroupId)) {
                removeClientServerGroupByUid(member.uuid, team.serverGroupId)
                    .catch(err => {
                        throw failedApiReply(err, 'removeUserFromTeam: Failed clearing ChannelGroup.');
                    })
            }

            return reply;
        })
        .catch(err => {
            throw failedApiReply(err, 'removeUserFromTeam: Failed setChannelGroup.');
        })

}


/**
 * Changes the name of the Team
 * @param {String} teamObjectID - Member objectID from the Database.
 * @param {String} name - New name for the team.
 */
const changeTeamName = (team, name) => {

    let dt = new Date();

    //Get Area ID to add the First Letter to the Name
    return gameArea.findById(team.areaId)
        .then((area) => {

            if (!area) {
                throw failedApiReply('Area Not Found', 'changeTeamName: No area Found!');
            }

            //making the name
            let processedName = '[cspacer' + team.spacerNumber + ']' + area.areaName[0] + team.channelOrder + ' - ★ ' + name + ' ★';

            //Edit the claimed channel
            return tsCore.edit(true, team.mainChannelId, processedName, '', 'topic', 'description')
                .then(() => {
                    if (team.serverGroupId !== null) {
                        return ts3.serverGroupRename(team.serverGroupId, name)
                            .then(() => {

                                Teams.findByIdAndUpdate(team._id,
                                    {
                                        $set: {
                                            teamName: name,
                                            nextNameChange: new Date(dt.getTime() + 1 * 60000) //720 Minutes
                                        }
                                    }, {new: true})
                                    .then(team => {
                                        return ApiReply('Name Changed Sucessfully', team)
                                    }).catch(err => {
                                    throw failedApiReply(err, 'changeTeamName: Failed to Saved to the DB');
                                })

                            })
                            .catch(err => {
                                throw failedApiReply(err, 'changeTeamName: Error Changing ServerGroup Name');
                            })
                    }
                    return ApiReply('Name Changed Sucessfully', team)
                })
        }).catch(err => {
            throw failedApiReply(err, 'changeTeamName: Error Changing Name');
        })
}


const createGroup = (name, type) => {


    return ts3.serverGroupCopy(config.groupSettings.gameServerGroupTemplate, 0, 1, name)
        .then(serverGroup => {

            //Add a new Entry to the Groups DB.
            let group = new Groups({
                serverGroupId: serverGroup.sgid,
                serverGroupName: name,
                groupType: type,
                members: []
            });

            group.save()

            //Save the New Channel
            return ApiReply('Group Created!', group);


        }).catch(err => {
            if (err.id === 1282) {
                throw failedApiReply(err, 'createGroup: That name is already in use!');
            }
            throw failedApiReply(err, 'createGroup: Failed to create or save a group');
        })
}




const addUserToGroup = (groupId, member) => {

    //Check if the user has linked his account to TeamSpeak

    //fix the Multiple add to the database/
    return Groups.findByIdAndUpdate(groupId,
        {
            $push: {
                members: {
                    memberId: member._id,
                    memberUuid: member.uuid
                }
            }
        })
        .then(group => {

            return setClientServerGroupByUid(member.uuid, group.serverGroupId)
                .then(info => {

                    return ApiReply('Group Created!', info);
                })
                .catch(err => {
                    throw failedApiReply(err, 'addUserToGroup: Failed add user to the serverGroup');
                })

        }).catch(err => {
            throw failedApiReply(err, 'addUserToGroup: Failed to fetch group database.');
        })

}

const removeUserToGroup = (groupId, member) => {

    //Check if the user has linked his account to TeamSpeak

    //save it on the database
    return Groups.findByIdAndUpdate(groupId,
        {
            $pull: {
                members: {
                    memberId: member._id,
                }
            }
        })
    //  Favorite.update( {cn: req.params.name}, { $pullAll: {uid: [req.params.deleteUid] } } )
        .then(group => {

            return removeClientServerGroupByUid(member.uuid, group.serverGroupId)
                .then(info => {

                    return ApiReply('Group Created!', info);
                })
                .catch(err => {
                    throw failedApiReply(err, 'addUserToGroup: Failed add user to the serverGroup');
                })

        }).catch(err => {
            throw failedApiReply(err, 'addUserToGroup: Failed to fetch group database.');
        })

}


const generateTeamDescription = (teamId) => {

    //Check if the user has linked his account to TeamSpeak

    return Teams.findById(teamId)
        .then(team => {

            topic = `Veja a pagina desta equipa em em: [url]deepgaming.pt/${team._id}[/url]`
            description = `Veja a Minha Pagina em: [url]deepgaming.pt/${team._id}[/url]`

            return tsCore.edit(false, team.mainChannelId, null, null, topic, description)
        })


}


module.exports = {
    // Team
    createTeam,
    changeUserPermissions,
    removeUserFromTeam,
    changeTeamName,
    createGroup,
    addUserToGroup,
    removeUserToGroup,
    generateTeamDescription,
    setSubChannelsPrivate,
    setSubChannelsPublic,
}