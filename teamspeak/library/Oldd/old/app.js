//Core
const ts3core = require("./ts3Connect") //Create and Claim 
const {ts3} = require("./ts3Connect")  //Dafault Library
const config = require('../../../config/ts3config'); //Objext that contains all the default properties to create a channel
const _ = require('lodash');

//Database
const gameArea = require('../../../database/models/game_area');
const Teams = require('../../../database/models/teams');
const Members = require('../../../database/models/members');
const Groups = require('../../../database/models/groups');

//////////////////////////////
//           Todos          //
//////////////////////////////
//Topic/Description Generators.
//Get all the code up to date with the new library.
//Improve database


//////////////////////////////
//      Resource Methods    //
//////////////////////////////

/**
 DONE:

 - Add User to the Channel Member *Done
 - Remove user from Channel Member *Done
 - Channel Crawl *Done
 - Move Channel *Done
 - Create Server Group *Done
 - Change Name *Done


 TODO NEXT TIME:


 - Delete Team
 -

 Todo Later:
 - Add more Channels X
 - Add Icon
 - Remove Icon


 Futuro

 Rank para Cada Team
 fazer pagina com o score e fazer website.

 Futuro erro
 Criar um canal kd o user ja nao esta o ts
 */

const failedApiReply = (error, msg) => {

    if (!error.error) {

        failedApi = {
            message: msg,
            error: error
        }
        return failedApi;
    }

    return error;
}

const ApiReply = (msg, data) => {

    api = {
        message: msg,
        data: data
    }

    return api;

}






const deleteBottonTeams = () => {


}










/**
 *
 * @param {string} nickname - Name of the user in the TeamSpeak
 * @param {String} uuid - TeamSpeak Client Unique ID
 */
const registerClient = (nickname, uuid) => {
    //Storing in the database details about the Channel and the Teamspeak Channel ids.
    let client = new Members({
        nickname: nickname,
        uuid: uuid
    });

    //Save the New Channel
    client.save()

    setClientServerGroupByUid(uuid, config.groupSettings.serverGroupMember)

}









////////////////////////////////////
//      Main/Exported Methods     //
////////////////////////////////////




module.exports = {

    // Users
    generateUserDescription,
    isMemberOnTheServer,
    moveToFirstChannel,
    sendTokenRequest,


    // Server
    serverView,
    getUsersByIp,
    serverInfo,


    // Channel
    createChannels,
    freeUpChanels,
    moveChannel,
    claimChannels,
    channelView,
    createGroupOfChannels,
    getTopFreeChannel,
    reassignChannelGroups,
    channelRemoveChannelGroupClients,
    getFreeTeams,
    crawlerChannels,

    // Team
    createTeam,
    addUserToTeam,
    removeUserFromTeam,
    changeTeamName,
    createGroup,
    addUserToGroup,
    removeUserToGroup,
    generateTeamDescription,
    setSubChannelsPrivate,
    setSubChannelsPublic,


    // tsServerGroups
    createServerGroup,
    setClientServerGroupByUid,
    removeClientServerGroupByUid,
    setClientChannelGroupUid,
    setChannelGroupbyUid,


    //Connection
    ts3
};


ts3.on("ready", () => {

})