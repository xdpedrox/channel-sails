const ts3 = require('../tsConnect');


/**
 * Creates a ServerGroup for the team.
 * @param {String} uuid - TeamSpeak Unique ID
 */
const createServerGroup = (team) => {

    //loop
    //Make a Copy of the template ServerGroup
    return ts3.serverGroupCopy(config.groupSettings.serverGroupTemplate, 0, 1, team.teamName)
        .then(group => {


            //Works Like a ForEach Loop but it's async
            let addMembersToGroupArray = team.members.map(teamMember => {


                //get cldbid of the user
                return tsCore.getCldbidFromUid(teamMember.memberUuid)
                    .then(cldbid => {

                        //Add ServerGroup to Client
                        return ts3.serverGroupAddClient(cldbid, group.sgid)
                            .then(() => {


                                return Teams.findByIdAndUpdate(team._id,
                                    {
                                        $set: {
                                            serverGroupId: group.sgid,
                                        }
                                    })
                                //Group was created sucessfully Return Sucessfull Message
                                    .then(() => {
                                        return ApiReply('ServerGroup Created Sucessfully', group.sgid)
                                    })

                                    .catch(err => {
                                        throw failedApiReply(err, 'createServerGroup: Error Saving data on the database');
                                    })

                            })
                            .catch(err => {
                                throw failedApiReply(err, 'createServerGroup: Error Adding user to group');
                            })

                    })
                    .catch(err => {
                        throw failedApiReply(err, 'createServerGroup: Error fetching cldbid ');
                    })

            });

            return Promise.all(addMembersToGroupArray)
                .then(array => {
                    return array;
                })
                .catch(err => {
                    throw failedApiReply(err, 'channelRemoveChannelGroupClients: Error Setting ChannelGroup.');
                })


        })
        .catch(err => {
            throw failedApiReply(err, 'createServerGroup: Error Copying Group');
        })

}


/**
 * Adds client to a ServerGroup
 * @param {String} ownerUuid - TeamSpeak User Unique Identifier
 * @param {Number} sgid - ServerGroup Id
 */
const setClientServerGroupByUid = (ownerUuid, sgid) => {

    //Get User DBID
    return tsCore.getCldbidFromUid(ownerUuid)
        .then(cldbid => {
            //Get list of SubChannels
            return ts3.serverGroupAddClient(cldbid, sgid)
                .then((data) => {
                    return ApiReply('setClientServerGroupByUid: Server Group Assigned', data)
                })
                .catch(err => {
                    throw failedApiReply(err, 'setClientServerGroupByUid: Failed to get cldbid');
                })

        })
        .catch(err => {
            throw failedApiReply(err, 'setClientServerGroupByUid: Failed to get cldbid');
        })
}



/**
 * Removes client from ServerGroup
 * @param {String} ownerUuid - TeamSpeak User Unique Identifier
 * @param {Number} sgid - ServerGroup ID
 */
const removeClientServerGroupByUid = (ownerUuid, sgid) => {

    //Get User DBID
    return tsCore.getCldbidFromUid(ownerUuid)
        .then(cldbid => {
            //Remove Server Group
            return ts3.serverGroupDelClient(cldbid, sgid)
                .then((data) => {
                    return ApiReply('removeClientServerGroupByUid: Success', data)
                })
        })
        .catch(err => {
            throw failedApiReply(err, 'removeClientServerGroupByUid: Failed to get cldbid');
        })
}


/**
 * Sets channelgroup to a user by is uuid
 * @param {Number} cgid - ChannelGroup ID.
 * @param {Number} mainChannelId - CID of the Main Channel.
 * @param {String} ownerUuid - TeamSpeak User Unique Identifier
 */
const setClientChannelGroupUid = (cgid, mainChannelId, ownerUuid) => {

    //Get User DBID
    return tsCore.getCldbidFromUid(ownerUuid)
        .then(cldbid => {
            //Get list of SubChannels
            return tsCore.subChannelList(mainChannelId)
                .then(channels => {

                    //Works Like a ForEach Loop but it's async
                    let promiseArr = channels.map(channel => {

                        //Set  Channel Group to Client
                        return ts3.setClientChannelGroup(cgid, channel._propcache.cid, cldbid)
                            .then(() => {
                                return channel._propcache.cid;
                            })
                            .catch(err => {
                                throw failedApiReply(err, 'setClientChannelGroupUid: Failed to get cldbid');
                            })
                    });

                    //Resolves and Checks if there was any problem with executiong returns results.
                    return Promise.all(promiseArr)
                        .then(resultsArray => {
                            return ApiReply('Channel Group Assigned', resultsArray)
                            // do something after the loop finishes
                        }).catch(err => {
                            throw failedApiReply(err, 'setChannelGroupbyUid: Error Setting ChannelGroup.');
                            // do something when any of the promises in array are rejected
                        })
                    //

                })
                .catch(err => {
                    throw failedApiReply(err, 'setClientChannelGroupUid: Failed to get cldbid');
                })
        })
        .catch(err => {
            throw failedApiReply(err, 'setClientChannelGroupUid: Failed to get cldbid');
        })
}


/**
 * Gives a user ChannelGroup Permission on all the subchannels.
 * @param {Number} sgid - ServerGroup ID
 * @param {Number} mainChannelId - Main id of the channel / spacer
 * @param {String} uuid  - TeamSpeak Client Unique ID
 */
const setChannelGroupbyUid = (sgid, mainChannelId, uuid) => {

    //Get the List of the SubChannels;
    return tsCore.subChannelList(mainChannelId)
        .then(channels => {

            //Get cldbid of User (Required to set Channel Admin)
            return tsCore.getCldbidFromUid(uuid)
                .then(cldbid => {

                    //Works Like a ForEach Loop but it's async
                    let promiseArr = channels.map(channel => {
                        //Set ChannelGroup to user
                        return ts3.setClientChannelGroup(sgid, channel._propcache.cid, cldbid)
                    });

                    //Resolves and Checks if there was any problem with executiong returns results.
                    return Promise.all(promiseArr)
                        .then(resultsArray => {
                            return ApiReply('Channel Group Assigned', resultsArray)
                            // do something after the loop finishes
                        }).catch(err => {
                            throw failedApiReply(err, 'setChannelGroupbyUid: Error Setting ChannelGroup.');
                            // do something when any of the promises in array are rejected
                        })
                    //
                })
                .catch(err => {
                    throw failedApiReply(err, 'setChannelGroupbyUid: Error getting cldbid of client.');
                })
        })
        .catch(err => {
            throw failedApiReply(err, 'setChannelGroupbyUid: Error Getting SubChannel List.');
        })
}

module.exports = {
    // tsServerGroups
    createServerGroup,
    setClientServerGroupByUid,
    removeClientServerGroupByUid,
    setClientChannelGroupUid,
    setChannelGroupbyUid,
}