const ts3 = require('../tsConnect');


const serverView = () => {

    return ts3.channelList()
        .then(channels => {

            return ts3.clientList()
                .then(clients => {

                    //Start
                    let promiseArr2 = channels.map(channel => {

                        let channelObject = {
                            cid: channel._propcache.cid,
                            pid: channel._propcache.pid,
                            channel_order: channel._propcache.channel_order,
                            channel_name: channel._propcache.channel_name,
                            channel_flag_password: channel._propcache.channel_flag_password,
                            channel_icon_id: channel._propcache.channel_icon_id,
                            total_clients: channel._propcache.total_clients

                        }

                        //START
                        let promiseArr = clients.map(client => {

                            return Members.findOne({uuid: client._propcache.client_unique_identifier})
                                .then(member => {

                                    UserObject = {};

                                    if (member) {

                                        let UserObject = {
                                            registered: true,
                                            _id: member._id,
                                            uuid: client._propcache.client_unique_identifier,
                                            clid: client._propcache.clid,
                                            cid: client._propcache.cid,
                                            client_nickname: client._propcache.client_nickname
                                        }

                                        return UserObject;

                                    } else {
                                        let UserObject = {
                                            registered: false,
                                            uuid: client._propcache.client_unique_identifier,
                                            clid: client._propcache.clid,
                                            cid: client._propcache.cid,
                                            client_nickname: client._propcache.client_nickname
                                        }
                                        return UserObject;

                                    }


                                })


                        })

                        //Resolves and Checks if there was any problem with executiong returns results.
                        return Promise.all(promiseArr)
                            .then(clientsArray => {

                                channelClients = clientsArray.filter(cl => channelObject.cid == cl.cid);

                                return channelClients

                            }).then(clients => {
                                channelObject.clients = clients

                                return channelObject;

                            }).catch(err => {
                                throw failedApiReply(err, 'API serverView: Error in the filtering process.');
                            })
                        //END

                    })


                    //Resolves and Checks if there was any problem with executiong returns results.
                    return Promise.all(promiseArr2)
                        .then(serverView => {

                            return serverView;

                            // do something after the loop finishes
                        }).catch(err => {
                            throw failedApiReply(err, 'API serverView: Failed to return data');
                        })

                }).catch(err => {
                    throw failedApiReply(err, 'API serverView: Failed to get clientList');
                })

        }).catch(err => {
            throw failedApiReply(err, 'API serverView: Failed to get channelList');
        })

}

const getUsersByIp = (clientIp) => {

    let filteredClients = [];

    return ts3.clientList()
        .then(clients => {

            let promiseArr = clients.map(client => {
                if (_.isEqual(client._propcache.connection_client_ip, clientIp)) {
                    filteredClients.push({
                        name: client._propcache.clid,
                        name: client._propcache.client_nickname,
                        uuid: client._propcache.client_unique_identifier
                    })
                }
            });

            //Resolves and Checks if there was any problem with executiong returns results.
            return Promise.all(promiseArr)
                .then(() => {

                    return ApiReply('Users fetched sucessfully', filteredClients);

                    // do something after the loop finishes
                }).catch(err => {
                    throw failedApiReply(err, 'getUsersByIp: Failed to get users from TeamSpeak');
                })
        }).catch(err => {
            throw failedApiReply(err, 'API getUsersByIp: Failed to get clientList');
        })

}


const serverInfo = () => {
    return ts3.serverInfo()
}

module.exports = {
   // Server
   serverView,
   getUsersByIp,
   serverInfo
}