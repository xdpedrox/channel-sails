const ts3 = require('../tsConnect');
const tsCore = require('./tsCore');
const ChannelGroup = require('./ChannelGroup')
const ServerGroup = require('./ServerGroup')
const Channel = require('./Channel')

const _ = require('lodash')
// const generateUserDescription()

/**
 * Uses the IP to check if user is connected to the teamspeak server
 * @param {String} ip
 */
const getConnectedUsersByIP = (ip) => {

  // Get client list with the filtered ip and type
  return ts3.clientList({
      connection_client_ip: ip,
      client_type: 0,
    })
    .then(clients => {
      return clients

    }).catch(err => {
      console.log(err)
      throw err

    })
}



/**
 * Moves client to the first channel of a team.
 * @param {Number} mainChannelId - CID of the Main Channel
 * @param {String} ownerUuid - TeamSpeak User Unique Identifier
 */
const moveUserToFirstSubChannel = (mainChannelId, clid) => {

  return tsCore.subChannelList(mainChannelId)
    .then(channels => {
      //Set Channel Group to all SubChannels
      return ts3.clientMove(clid, channels[0]._propcache.cid)
        .then(data => {
          return true
        })
        .catch(err => {
          // Member is already member of that channel
          if (err.id = 770) {
            return true;
          } else {
            throw err
          }
          // failedApiReply(err, 'moveToFirstChannel: Failed to Move Client');
        })
    })
    .catch(err => {
      throw err
      // failedApiReply(err, 'moveToFirstChannel: Failed to get subChannelList');
    })
}

/**
 * Moves client to the first channel of a team.
 * @param {Number} mainChannelId - CID of the Main Channel
 * @param {String} ownerUuid - TeamSpeak User Unique Identifier
 */
const moveToFirstChannel = (mainChannelId, clid) => {

  return tsCore.subChannelList(mainChannelId)
    .then(channels => {
      //Set Move Client
      return ts3.clientMove(clid, channels[0]._propcache.cid)
        .catch(err => {
          if (err.id == 770) {
            return "User is already in the channel"
          } else {
            throw err;
          }
          //   failedApiReply(err, 'moveToFirstChannel: Failed to Move Client');
        })
    })
    .catch(err => {
      // console.log('lol')
      throw err;
      // failedApiReply(err, 'moveToFirstChannel: Failed to get subChannelList');
    })
}


module.exports = {
  //   generateUserDescription,
  // getConnectedUsersByIP,
  // moveUserToFirstSubChannel,
  // sendTokenRequest
}
