const ts3 = require('../tsConnect');
const tsCore = require('./tsCore');
const ServerGroup = require('./ServerGroup')
const User = require('./User')
const Channel = require('./Channel')




/**
 * Gives a user ChannelGroup Permission on all the subchannels.
 * @param {Number} sgid - ServerGroup ID
 * @param {Number} mainChannelId - Main id of the channel / spacer
 * @param {String} uuid  - TeamSpeak Client Unique ID
 */
const setChannelGroupbyUid = (sgid, mainChannelId, uuid) => {

    //Get the List of the SubChannels;
    return tsCore.subChannelList(mainChannelId)
      .then(channels => {
        //Get cldbid of User (Required to set Channel Admin)
        return tsCore.getCldbidFromUid(uuid)
          .then(cldbid => {
            //Works Like a ForEach Loop but it's async
            let promiseArr = channels.map(channel => {
              //Set ChannelGroup to user
              return ts3.setClientChannelGroup(sgid, channel._propcache.cid, cldbid)
            });
  
            //Resolves and Checks if there was any problem with executiong returns results.
            return Promise.all(promiseArr)
              .then(resultsArray => {
                return resultsArray;
                // return ApiReply('Channel Group Assigned', resultsArray)
                // do something after the loop finishes
              }).catch(err => {
                throw err
                // failedApiReply(err, 'setChannelGroupbyUid: Error Setting ChannelGroup.');
                // do something when any of the promises in array are rejected
              })
            //
          })
          .catch(err => {
            throw err
            // failedApiReply(err, 'setChannelGroupbyUid: Error getting cldbid of client.');
          })
      })
      .catch(err => {
        throw err
        // failedApiReply(err, 'setChannelGroupbyUid: Error Getting SubChannel List.');
      })
  }
  