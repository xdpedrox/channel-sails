const ts3 = require('../tsConnect');
const tsCore = require('./tsCore');
const ChannelGroup = require('./ChannelGroup')
const Channel = require('./Channel')
const User = require('./User')


/**
 * Adds client to a ServerGroup
 * @param {String} ownerUuid - TeamSpeak User Unique Identifier
 * @param {Number} sgid - ServerGroup Id
 */
const addServerGroup = (ownerUuid, sgid) => {

    //Get User DBID
    return tsCore.getCldbidFromUid(ownerUuid)
      .then(cldbid => {
        //Get list of SubChannels
        return ts3.serverGroupAddClient(cldbid, sgid)
          .then((data) => {
            return data;
          })
          .catch(err => {
            throw err;
            // throw failedApiReply(err, 'setClientServerGroupByUid: Failed to get cldbid');
          })
  
      })
      .catch(err => {
        throw err;
        // throw failedApiReply(err, 'setClientServerGroupByUid: Failed to get cldbid');
      })
  }


  /**
 * Removes client from ServerGroup
 * @param {String} ownerUuid - TeamSpeak User Unique Identifier
 * @param {Number} sgid - ServerGroup ID
 */
const removeServerGroup = (ownerUuid, sgid) => {

    //Get User DBID
    return tsCore.getCldbidFromUid(ownerUuid)
        .then(cldbid => {
            //Remove Server Group
            return ts3.serverGroupDelClient(cldbid, sgid)
                .then((data) => {
                    return ApiReply('removeClientServerGroupByUid: Success', data)
                })
        })
        .catch(err => {
            throw failedApiReply(err, 'removeClientServerGroupByUid: Failed to get cldbid');
        })
}