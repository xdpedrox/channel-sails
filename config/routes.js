/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  //Auth
  'GET /auth/steam':                  { action: 'auth/steam' },
  'GET /auth/steam/callback':         { action: 'auth/steam-callback' },

  '/':                                { view: 'pages/index' },
  'GET /teamspeak/serverinfo':        { action: 'teamspeak/serverinfo' },
  'GET /teamspeak/connectedclients':  { action: 'teamspeak/get-connected-clients' },
  'GET /test':                        { action: 'team/test' },
  // Team
  'PUT /team':                        { action: 'team/create-team' },


  // 'GET /team/new':                 { view: 'pages/team/new' },




  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
